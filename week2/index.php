<!DOCTYPE html>
<html lang="en">
<head>
  <title>Chhinh Sovath</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
 
<div class="container">
  <h2>Week 2 Assignment #2</h2>
  <div class="panel-group">
    <div class="panel panel-primary">
      <div class="panel-heading">Exercise Number 1</div>
      <div class="panel-body">
          
      1- Write a program to reverse this string from “emocleW ot PHP” to “Welcome to PHP”
      <a class="btn btn-primary" href="1.php" role="button">Exercise Number 1</a>

      </div>
    </div>

    <div class="panel panel-success">
      <div class="panel-heading">Exercise Number 2</div>
      <div class="panel-body">
          2- There is an indexed array $arr = [2, 3, 4, 6, 7, 9, 11, 20]. <br>Write a php program to filter this array by even number using arrow function.
          <a class="btn btn-primary" href="2.php" role="button">Exercise Number 2</a>

      </div>
    </div>

    <div class="panel panel-info">
      <div class="panel-heading">Exercise Number 3</div>
      <div class="panel-body">
            <p>
                        3- We want to implement a function that could sum all provided values as below:
                        <br>
                        $total = sum (2, 3) then $total = 5 
                        <br>
                        $total = sum (2, 3, 4) then $total = 9 
                        <br>
                        $total = sum (2, 3, 4, 5) then $total = 14
            </p>
            <a class="btn btn-primary" href="3.php" role="button">Exercise Number 3</a>
      </div>
    </div>
  </div>
</div>

</body>
</html>
