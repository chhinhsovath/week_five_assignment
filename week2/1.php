<!DOCTYPE html>
<html lang="en">
<head>
  <title>Chhinh Sovath</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="index.php">Week 2 Assignment #2</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="1.php">Exercise Number 1</a></li>
      <li><a href="2.php">Exercise Number 2</a></li>
      <li><a href="3.php">Exercise Number 3</a></li>
    </ul>
  </div>
</nav>
    <?php 
    if ( isset($_POST['form_source'])) {
            if (
                isset($_POST['string']) && !empty($_POST['string']) 
            ) {
                $result ="";
                // echo 'Redirect to thank you';
                $string = $_POST['string'];
                // echo "Entered String: <b>" . $string . "</b><br>"; 
                // find string length
                $str_len 	= strlen($string);

                // echo "Reversed String: "; 
                // loop through it and print it turn around
                for ( $x = $str_len - 1; $x >=0; $x-- )
                {
                $result .= $string[$x];
                }
                // echo '<b>'.$result.'</b>';
                header('Location: 1.php?result=true&orignal='.$string.'&reverse='.$result);
                exit();
            } else {
                // echo 'Redirect to contact';
                header('Location: 1.php?error=1');
                exit();
            }
        }
    ?>
        <div class="container">
            
            <div class="col-4">
                1- Write a program to reverse this string from “emocleW ot PHP” to “Welcome to PHP”
            </div>
            <div class="col-4">
            <h1></h1>
                <form method="POST" action="1.php">
                    <input type="hidden" name="form_source" value="strReverse">
                    
                    <?php
                        if (isset($_GET['error'])) {
                    ?>       
                            <div class="form-group">
                                <label>String are required</label>
                            </div>
                    <?php
                        } elseif (isset($_GET['result'])) {
                     ?>       
                            <hr>
                            <div class="form-group">
                                <label>Original String :</label> <b><?php echo $_REQUEST['orignal']; ?></b>
                            </div>
                            <div class="form-group">
                                <label>Reverse String :</label> <b><?php echo $_REQUEST['reverse']; ?></b>
                            </div>
                    <?php        
                        }
                    ?>
                    
                    <div class="form-group">
                        <label for="message">String</label>
                        <textarea class="form-control" name="string" id="string" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>

    </body>
</html>