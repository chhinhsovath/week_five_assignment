<!DOCTYPE html>
<html lang="en">
<head>
  <title>Chhinh Sovath</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="index.php">Week 2 Assignment #2</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="1.php">Exercise Number 1</a></li>
      <li><a href="2.php">Exercise Number 2</a></li>
      <li class="active"><a href="3.php">Exercise Number 3</a></li>
    </ul>
  </div>
</nav>    
    <?php

        if (isset($_POST['val1']) && !empty($_POST['val1'])) { $number1 = $_POST['val1']; } else {$number1 = 0;}
        if (isset($_POST['val2']) && !empty($_POST['val2'])) { $number2 = $_POST['val2']; } else {$number2 = 0;}
        if (isset($_POST['val3']) && !empty($_POST['val3'])) { $number3 = $_POST['val3']; } else {$number3 = 0;}
        if (isset($_POST['val4']) && !empty($_POST['val4'])) { $number4 = $_POST['val4']; } else {$number4 = 0;}

        // if($number1==0 && $number2==0 && $number3==0 && $number3==0){ 
            
        //     header('Location: 3.php?error=1');
        //     exit();
        // }

        function sum()
        {
            $numbers = func_get_args();
            $total = 0;
            for ($i = 0; $i < count($numbers); $i++) {
                $total += $numbers[$i];
            }
            return $total;
        }
    ?>
        <div class="container">

                        <p>
                        3- We want to implement a function that could sum all provided values as below:
                        <br>
                        $total = sum (2, 3) then $total = 5 
                        <br>
                        $total = sum (2, 3, 4) then $total = 9 
                        <br>
                        $total = sum (2, 3, 4, 5) then $total = 14
                        <br>
                        </p>
                        <hr>
                        <?php
                        echo '<h2>Static Numbers<br></h2>';
                        echo 'sum (2, 3) ='.sum(2, 3) . '<br>'; // 5
                        echo 'sum(2, 3, 4) ='.sum(2, 3, 4) . '<br>'; // 9
                        echo 'sum (2, 3, 4, 5) ='. sum (2, 3, 4, 5) . '<br>'; //14
                        echo '<h2>Dynamic Numbers</h2>';
                        echo 'Val1 + Val2 = '.sum($number1, $number2, ) . '<br>'; // 9
                        echo 'Val1 + Val2 + Val3 = '.sum($number1, $number2, $number3) . '<br>'; // 9
                        echo 'Val1 + Val2 + Val3 + Val4 = '.sum($number1, $number2, $number3,$number4) . '<br>'; // 9

                        ?>
                        <hr>
                        <form method="POST" action="3.php" id="myForm">
                            <input type="hidden" name="form_source" value="strReverse">
                            <div class="form-group">
                                <label for="message">Vaue 1</label>
                                <input type="text" class="form-control" name="val1" id="val1" value="<?php echo $number1;?>"  required></input>
                            </div>
                            <div class="form-group">
                                <label for="message">Vaue 2</label>
                                <input type="text" class="form-control" name="val2" id="val2" value="<?php echo $number2;?>"  required></input>
                            </div>
                            <div class="form-group">
                                <label for="message">Vaue 3</label>
                                <input type="text" class="form-control" name="val3" id="val3" value="<?php echo $number3;?>"  required></input>
                            </div>
                            <div class="form-group">
                                <label for="message">Vaue 4</label>
                                <input type="text" class="form-control" name="val4" id="val4" value="<?php echo $number4;?>"  required></input>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" onclick="myFunction()" class="btn btn-default">Reset</button>
                        </form>
            
        </div>

        <script>
        function myFunction() {
            // document.getElementById("myForm").reset();
            $("#myForm input").val('');
        }
        </script>
    </body>
</html>