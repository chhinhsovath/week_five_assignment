<!DOCTYPE html>
<html lang="en">
<head>
  <title>Chhinh Sovath</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="index.php">Week 2 Assignment #2</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="1.php">Exercise Number 1</a></li>
      <li class="active"><a href="2.php">Exercise Number 2</a></li>
      <li><a href="3.php">Exercise Number 3</a></li>
    </ul>
  </div>
</nav>
    <?php 
    if ( isset($_POST['form_source'])) {
            if (
                isset($_POST['arraynumber']) && !empty($_POST['arraynumber']) 
            ) {
                $orignal =  $_POST['arraynumber'];
                $str_arr = explode (",", $orignal); 

                    $even ="";
                    foreach ($str_arr as $value) {
                        
                        if($value % 2 == 0){
                            $even .=$value .' - '; 
                        }
                    }
                // echo 'Redirect and put the result    
                header('Location: 2.php?result=true&orignal='.$orignal.'&even='.$even);
                exit();
            } else {
                // echo 'Redirect to get input text;
                header('Location: 2.php?error=1');
                exit();
            }
        }
    ?>
        <div class="container">
                            
            2- There is an indexed array $arr = [2, 3, 4, 6, 7, 9, 11, 20]. <br>Write a php program to filter this array by even number using arrow function.
            <hr>

            <h2>Results from Static Numbers using Arrow Function</h2>
           <?php
 
                $arr = [2, 3, 4, 6, 7, 9, 11, 20];
                $result = array_filter($arr, fn($n) => $n%2 == 0);
                $even ="";
                foreach ($result as $value) {
                    $even .= $value.'-';
                }
                echo 'Print only value <br>';
                echo $even;
                echo '<br><hr>';
                echo 'Print in array format <br>';
                print_r($result);

            ?>

            <h2>Results from Static Numbers my my self</h2>
            <?php 
            $arr = [2, 3, 4, 6, 7, 9, 11, 20];

                foreach ($arr as $value) {
                        $even ="";
                    
                    if($value % 2 == 0){
                        $even .=$value.' - '; 
                    }
                        echo $even;
                }
            ?>
            <h2>Results from Dynamic Numbers</h2>
            <?php
                    if (isset($_GET['error'])) {
                    ?>       
                            <div class="form-group">
                                <label>Numers are required</label>
                            </div>
                    <?php
                        } elseif (isset($_GET['result'])) {
                     ?>       
                            <div class="form-group">
                                <label>Original Numers :</label> <b><?php echo $_REQUEST['orignal']; ?></b>
                            </div>
                            <div class="form-group">
                                <label>Filter Even Numers :</label> <b><?php echo $_REQUEST['even']; ?></b>
                            </div>
                    <?php        
                        }
                    ?>
            <hr>
                <form method="POST" action="2.php">
                    <input type="hidden" name="form_source" value="arraynumber">
                    <div class="form-group">
                        <label for="message">Numbers seperate by comma "," </label>
                        <input type="text" class="form-control" name="arraynumber" id="arraynumber"  required></input>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
        </div>

    </body>
</html>